# What

Shell script to export metrics for prometheus node_exporter

## Who

### prom-proc

Gather processus informations: per process: rss, vsz and uss, cpu system and user. And sum of rss, vsz, uss.

#### Usage

```
prom-proc <prometheus metric name> "<process as per 'ps -aeo command'>[#label]"
```

#### Examples

```
$ sudo prom-proc node_alliance_proc "php-fpm:#php"
node_alliance_proc_memory_rss_bytes{cmd="php",type="rss",pid="441",index="0"} 5029888
node_alliance_proc_memory_vsz_bytes{cmd="php",type="vsz",pid="441",index="0"} 283025408
node_alliance_proc_memory_uss_bytes{cmd="php",type="uss",pid="441",index="0"} 69632
node_alliance_proc_cpu_user_percent{cmd="php",pid="441"} 0.004157
node_alliance_proc_cpu_system_percent{cmd="php",pid="441"} 0.007196
node_alliance_proc_cpu_total_percent{cmd="php",pid="441"} 0.011353
node_alliance_proc_memory_rss_bytes{cmd="php",type="rss",pid="603",index="1"} 12562432
node_alliance_proc_memory_vsz_bytes{cmd="php",type="vsz",pid="603",index="1"} 293105664
node_alliance_proc_memory_uss_bytes{cmd="php",type="uss",pid="603",index="1"} 2965504
node_alliance_proc_cpu_user_percent{cmd="php",pid="603"} 0.006481
node_alliance_proc_cpu_system_percent{cmd="php",pid="603"} 0.003316
node_alliance_proc_cpu_total_percent{cmd="php",pid="603"} 0.009797
node_alliance_proc_count{cmd="php"} 2
node_alliance_proc_memory_rss_sum_bytes{cmd="php"} 13065420
node_alliance_proc_memory_vsz_sum_bytes{cmd="php"} 576131072
node_alliance_proc_memory_uss_sum_bytes{cmd="php"} 3035136
node_alliance_proc_memory_rss_bytes{cmd="/lib/systemd/systemd",type="rss",pid="738",index="0"} 3870720
node_alliance_proc_memory_vsz_bytes{cmd="/lib/systemd/systemd",type="vsz",pid="738",index="0"} 12824576
node_alliance_proc_memory_uss_bytes{cmd="/lib/systemd/systemd",type="uss",pid="738",index="0"} 884736
node_alliance_proc_cpu_user_percent{cmd="/lib/systemd/systemd",pid="738"} 0.000074
node_alliance_proc_cpu_system_percent{cmd="/lib/systemd/systemd",pid="738"} 0.000040
node_alliance_proc_cpu_total_percent{cmd="/lib/systemd/systemd",pid="738"} 0.000114
node_alliance_proc_count{cmd="/lib/systemd/systemd"} 1
node_alliance_proc_memory_rss_sum_bytes{cmd="/lib/systemd/systemd"} 3870720
node_alliance_proc_memory_vsz_sum_bytes{cmd="/lib/systemd/systemd"} 12824576
node_alliance_proc_memory_uss_sum_bytes{cmd="/lib/systemd/systemd"} 884736
```

